package deltanedas.thezoneproject;

public class Constants {
	public static final String MOD_ID = "thezoneproject";
	public static final String MOD_NAME = "The Zone Project";
	public static final String MOD_VERSION = "__MOD_VERSION";
	public static final String MC_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY = "deltanedas.thezoneproject.proxies.ClientProxy";
	public static final String COMMON_PROXY = "deltanedas.thezoneproject.proxies.CommonProxy";
}
